import { GameMode } from "../server";
import { playerCounts } from "./settings";

describe("player counts", () => {
  test("removes impossible player counts", () => {
    let counts = playerCounts({
      mode: GameMode.Classic,
      selectedPlayerCounts: [1, 2, 3, 4, 5],
    });

    expect(counts).toEqual([2, 3, 4]);
  });
  test("removes unselected player counts", () => {
    let counts = playerCounts({
      mode: GameMode.Modern,
      selectedPlayerCounts: [3, 4],
    });
    expect(counts).toEqual([3, 4]);
  });
});
