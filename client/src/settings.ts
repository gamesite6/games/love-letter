import { GameMode } from "../server";
import type { Settings } from "../server";

export const defaultMode: GameMode = GameMode.Modern;

export const possiblePlayerCounts = {
  Modern: [2, 3, 4, 5, 6],
  Classic: [2, 3, 4],
};

export const defaultSettings: Settings = {
  mode: defaultMode,
  selectedPlayerCounts: possiblePlayerCounts[defaultMode],
};

export function playerCounts(settings: Settings): number[] {
  const possible = possiblePlayerCounts[settings.mode];
  return possible.filter((c) => settings.selectedPlayerCounts.includes(c));
}
