import { CardFace } from "../server";

export default function emoji(card: CardFace): string {
  switch (card) {
    case CardFace.C9:
      return "👩🏻‍🎤";
    case CardFace.C8:
      return "👨🏼‍💼";
    case CardFace.C7:
      return "👨🏾‍💻";
    case CardFace.C6:
      return "👨🏼‍🏫";
    case CardFace.C5:
      return "🧑🏻‍🔧";
    case CardFace.C4:
      return "👩🏼‍⚕️";
    case CardFace.C3:
      return "🧑🏿‍⚖️";
    case CardFace.C2:
      return "🕵🏻‍♀️";
    case CardFace.C1:
      return "👮🏾‍♀️";
    case CardFace.C0:
      return "🥷🏼";
  }
}
