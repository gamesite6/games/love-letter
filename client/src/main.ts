import type { Settings as SettingsType, State, PlayerId } from "../server";
import GameComponent from "./Game.svelte";
import ReferenceComponent from "./Reference.svelte";
import SettingsComponent from "./Settings.svelte";

import "@fontsource/berkshire-swash";

type GameProps = {
  playerId?: PlayerId;
  state: State;
  settings: SettingsType;
};

export class Game extends HTMLElement {
  #playerId?: PlayerId;
  #state?: State;
  #settings?: SettingsType;
  #app?: GameComponent;

  set state(state: State) {
    if (this.#state !== state) {
      this.#state = state;
      this.rerender({ state });
    }
  }

  set settings(settings: SettingsType) {
    if (this.#settings !== settings) {
      this.#settings = settings;
      this.rerender({ settings });
    }
  }

  set playerid(playerId: PlayerId | undefined) {
    if (this.#playerId !== playerId) {
      this.#playerId = playerId;
      this.rerender({ playerId });
    }
  }

  connectedCallback() {
    if (this.#settings && this.#state) {
      this.#app = new GameComponent({
        target: this,
        props: {
          playerId: this.#playerId,
          settings: this.#settings,
          state: this.#state,
        },
      });

      this.#app.$on("action", ({ detail }) => {
        this.dispatchEvent(new CustomEvent("action", { detail }));
      });
    }
  }

  private rerender(changedProps: Partial<GameProps>) {
    if (this.#app) {
      this.#app.$set(changedProps);
    }
  }
}

export class Reference extends HTMLElement {
  #settings?: SettingsType;
  #app?: ReferenceComponent;

  set settings(settings: SettingsType) {
    if (this.#settings !== settings) {
      this.#settings = settings;
      this.rerender();
    }
  }

  connectedCallback() {
    if (this.#settings) {
      this.#app = new ReferenceComponent({
        target: this,
        props: {
          settings: this.#settings,
        },
      });
    }
  }

  private rerender() {
    if (this.#app) {
      this.#app.$set({
        settings: this.#settings,
      });
    }
  }
}

export { defaultSettings, playerCounts } from "./settings";

type SettingsProps = {
  settings?: SettingsType;
  readonly?: boolean;
};

export class Settings extends HTMLElement {
  #app?: SettingsComponent;

  #settings?: SettingsType;

  set settings(settings: SettingsType) {
    if (this.#settings !== settings) {
      this.#settings = settings;
      this.rerender({ settings });
    }
  }

  get #readonly(): boolean {
    const attr = this.attributes.getNamedItem("readonly");
    return attr !== null && attr.value !== "false";
  }

  connectedCallback() {
    this.#app = new SettingsComponent({
      target: this,
      props: {
        readonly: this.#readonly,
        settings: this.#settings,
      },
    });

    this.#app.$on("settings_change", (evt) => {
      this.dispatchEvent(
        new CustomEvent("settings_change", {
          detail: evt.detail,
        })
      );
    });
  }

  private rerender(changedProps: Partial<SettingsProps>) {
    if (this.#app) {
      this.#app.$set(changedProps);
    }
  }

  static get observedAttributes() {
    return ["readonly"];
  }

  attributeChangedCallback(name: string, oldValue: any, newValue: any) {
    if (name === "readonly") {
      this.rerender({ readonly: this.#readonly });
    }
  }
}
