import type { PlayerId, Phase } from "../../server";

function assertNever(t: never): never {
  throw new Error();
}
export function isActing(phase: Phase, playerId: PlayerId): boolean {
  switch (phase.kind) {
    case "Choose":
    case "Guess":
    case "Peek":
    case "PeekComplete":
    case "Compare":
    case "Discard":
    case "Draw2":
    case "Swap":
      return phase.actor === playerId;

    case "CompareComplete":
      return (
        (phase.actor === playerId && !phase.actor_ready) ||
        (phase.target === playerId && !phase.target_ready)
      );
    case "RoundComplete":
      return !phase.ready.includes(playerId);
    case "GameComplete":
      return false;

    default:
      assertNever(phase);
  }
}
