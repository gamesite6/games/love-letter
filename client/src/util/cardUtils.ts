import type { CardId } from "../../server";
import { CardFace } from "../../server";

export function isCardFace(card: CardId | CardFace): card is CardFace {
  return typeof card === "string";
}

export function isCardId(card: CardId | CardFace): card is CardId {
  return typeof card === "number";
}

export function cardTitle(card: CardId | CardFace): string {
  if (isCardId(card)) {
    card = cardFace(card);
  }

  switch (card) {
    case CardFace.C9:
      return "Rock star";
    case CardFace.C8:
      return "Manager";
    case CardFace.C7:
      return "Hacker";
    case CardFace.C6:
      return "Teacher";
    case CardFace.C5:
      return "Mechanic";
    case CardFace.C4:
      return "Doctor";
    case CardFace.C3:
      return "Judge";
    case CardFace.C2:
      return "Detective";
    case CardFace.C1:
      return "Security";
    case CardFace.C0:
      return "Ninja";
  }
}

export function cardFace(cardId: CardId): CardFace {
  if (cardId < 10) {
    return CardFace.C0;
  } else if (cardId < 20) {
    return CardFace.C1;
  } else if (cardId < 30) {
    return CardFace.C2;
  } else if (cardId < 40) {
    return CardFace.C3;
  } else if (cardId < 50) {
    return CardFace.C4;
  } else if (cardId < 60) {
    return CardFace.C5;
  } else if (cardId < 70) {
    return CardFace.C6;
  } else if (cardId < 80) {
    return CardFace.C7;
  } else if (cardId < 90) {
    return CardFace.C8;
  } else {
    return CardFace.C9;
  }
}

export function cardValue(card: CardId | CardFace): number {
  if (isCardId(card)) {
    card = cardFace(card);
  }

  switch (card) {
    case CardFace.C9:
      return 9;
    case CardFace.C8:
      return 8;
    case CardFace.C7:
      return 7;
    case CardFace.C6:
      return 6;
    case CardFace.C5:
      return 5;
    case CardFace.C4:
      return 4;
    case CardFace.C3:
      return 3;
    case CardFace.C2:
      return 2;
    case CardFace.C1:
      return 1;
    case CardFace.C0:
      return 0;
  }
}

export function cardDescription(cardFace: CardFace): string {
  switch (cardFace) {
    case CardFace.C9:
      return "If you play or discard this card you are out of the round.";
    case CardFace.C8:
      return `If the ${cardTitle(CardFace.C7)} or ${cardTitle(
        CardFace.C5
      )} is in your hand, you must play this card.`;
    case CardFace.C7:
      return "Swap hands with another player.";
    case CardFace.C6:
      return "Draw 2 cards. Put 2 cards back at the bottom of the deck in any order.";
    case CardFace.C5:
      return "Choose a player who must discard their hand and redraw.";
    case CardFace.C4:
      return "Until your next turn other players may not target you.";
    case CardFace.C3:
      return "Secretly compare hands with another player. Whoever has the lower value hand is out of the round.";
    case CardFace.C2:
      return "Peek at another player's hand.";
    case CardFace.C1:
      return `Guess another player's card (except ${cardTitle(
        CardFace.C1
      )}). If you guess correctly they are out of the round.`;
    case CardFace.C0:
      return `At the end of the round, if you are the only remaining player who played or discarded a ${cardTitle(
        CardFace.C0
      )} in your discards you score 1 point.`;
  }
}
