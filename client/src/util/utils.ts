import type { Player, PlayerId } from "../../server";

export default function orderPlayers(
  allPlayers: Player[],
  playerId: PlayerId | undefined
): Player[] {
  let playerIndex = allPlayers.findIndex((player) => player.id === playerId);

  if (playerIndex === -1) {
    return allPlayers;
  } else {
    let player = allPlayers[playerIndex];

    let playersBeforePlayer = allPlayers.slice(0, playerIndex);
    let playersAfterPlayer = allPlayers.slice(playerIndex + 1);

    return [player, ...playersAfterPlayer, ...playersBeforePlayer];
  }
}

export function range(from: number, to: number): number[] {
  const size = Math.max(to - from, 0);
  const result = new Array(size);

  for (let i = 0; i < size; i++) {
    result[i] = from + i;
  }

  return result;
}

export function rangeInclusive(from: number, to: number): number[] {
  return range(from, to + 1);
}

export function reversed<T>(ts: T[]): T[] {
  let result = [...ts];
  result.reverse();
  return result;
}

export function indexed<T>(ts: T[]): [T, number][] {
  return ts.map((t, i) => [t, i]);
}
