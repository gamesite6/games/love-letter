import { createEventDispatcher } from "svelte";
import type { Action } from "../../server";

export function createActionDispatcher(): (action: Action) => void {
  const dispatch = createEventDispatcher();

  return (action: Action) => {
    dispatch("action", action);
  };
}
