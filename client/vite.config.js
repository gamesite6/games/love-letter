import path from "path";
import { defineConfig } from "vite";
import { svelte } from "@sveltejs/vite-plugin-svelte";

// https://vitejs.dev/config/
export default defineConfig({
  base: "/static/fan-mail/",
  build: {
    lib: {
      entry: path.resolve(__dirname, "src", "main.ts"),
      fileName: "fan-mail",
      name: "Gamesite6_FanMail",
    },
  },
  css: {
    modules: {},
  },
  plugins: [svelte()],
});
