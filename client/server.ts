export type PlayerId = number;

export type CardId = number;

export type Settings = { mode: GameMode; selectedPlayerCounts: number [] };

export enum GameMode { Classic = "Classic", Modern = "Modern" };

export type Action = 
 | { kind: "Choose"; played: number } 
 | { kind: "Swap"; target: PlayerId } 
 | { kind: "Draw2"; returned: number } 
 | { kind: "Discard"; target: PlayerId } 
 | { kind: "Compare"; target: PlayerId } 
 | { kind: "CompareComplete" } 
 | { kind: "Peek"; target: PlayerId } 
 | { kind: "PeekComplete" } 
 | { kind: "Guess"; target: PlayerId; guess: CardFace } 
 | { kind: "RoundComplete" };

export type State = {     deck: Deck; set_aside: CardId; face_up: CardId [] | null; players:     Player []; phase: Phase; speech: { [playerId: number]: Speech } };

export type Player = {     id: PlayerId; score: number; hand: CardId []; discards: CardId [];     protected: boolean };

export type Speech = 
 | { kind: "PlayCard"; card: CardFace } 
 | { kind: "NoEffect"; card: CardFace } 
 | { kind: "Guess"; target: PlayerId; guess: CardFace } 
 | { kind: "GuessResponse"; correct: boolean } 
 | { kind: "Peek"; target: PlayerId } 
 | { kind: "Compare"; target: PlayerId } 
 | { kind: "Discard"; target: PlayerId } 
 | { kind: "Swap"; target: PlayerId };

export type Deck = CardId [];

export type Phase = 
 | { kind: "Choose"; actor: PlayerId } 
 | { kind: "Guess"; actor: PlayerId } 
 | { kind: "Peek"; actor: PlayerId } 
 | {     kind: "PeekComplete"; actor: PlayerId; target: PlayerId; card:     CardFace } 
 | { kind: "Compare"; actor: PlayerId } 
 | {     kind: "CompareComplete"; actor: PlayerId; actor_ready: boolean;     target: PlayerId; target_ready: boolean } 
 | { kind: "Discard"; actor: PlayerId } 
 | { kind: "Draw2"; actor: PlayerId } 
 | { kind: "Swap"; actor: PlayerId } 
 | {     kind: "RoundComplete"; winners: PlayerId []; spy: PlayerId | null;     ready: PlayerId [] } 
 | { kind: "GameComplete" };

export enum CardFace {     C0 = "C0", C1 = "C1", C2 = "C2", C3 = "C3", C4 = "C4", C5 = "C5", C6 =     "C6", C7 = "C7", C8 = "C8", C9 = "C9" };

