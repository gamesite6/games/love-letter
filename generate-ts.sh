#!/bin/bash
set -e

echo "Generating typescript file ..."
(cd server && cargo run --bin generate-ts) > client/server.ts

echo "Type checking generated file ..."
(cd client && npx tsc --noEmit ./server.ts)

echo "Done!"
