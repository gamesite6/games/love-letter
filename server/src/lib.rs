pub mod action;
pub mod card;
pub mod settings;
pub mod state;

use rand::prelude::*;
pub use settings::{GameMode, Settings};
use state::Deck;
use state::Phase;
use state::Player;
use state::State;
use std::collections::{HashMap, HashSet};
use std::iter::FromIterator;

pub type PlayerId = u32;

fn allowed_player_counts(mode: &GameMode) -> HashSet<usize> {
    match &mode {
        GameMode::Classic => HashSet::from_iter([2, 3, 4]),
        GameMode::Modern => HashSet::from_iter([2, 3, 4, 5, 6]),
    }
}

pub fn player_counts(settings: Settings) -> HashSet<usize> {
    settings
        .selected_player_counts
        .intersection(&allowed_player_counts(&settings.mode))
        .copied()
        .collect()
}

pub fn initial_state(settings: Settings, player_ids: Vec<PlayerId>, seed: i64) -> Option<State> {
    let mut rng = rand_pcg::Pcg32::seed_from_u64(seed.unsigned_abs());

    let player_count = player_ids.len();
    if !allowed_player_counts(&settings.mode).contains(&player_count) {
        return None;
    }

    let mut deck = Deck::new(&settings.mode, &mut rng);
    let set_aside = deck.draw_card()?;

    let face_up = if player_count == 2 {
        Some([deck.draw_card()?, deck.draw_card()?, deck.draw_card()?])
    } else {
        None
    };

    let mut players = Vec::with_capacity(player_ids.len());
    for &player_id in player_ids.iter() {
        let hand = deck.draw_card()?;
        players.push(Player::new(player_id, hand));
    }

    let first_player: &mut Player = players.first_mut()?;
    let drawn_card = deck.draw_card()?;
    first_player.hand.push(drawn_card);

    let phase = Phase::Choose {
        actor: first_player.id,
    };

    Some(State {
        deck,
        set_aside,
        face_up,
        players,
        phase,
        speech: HashMap::new(),
    })
}
