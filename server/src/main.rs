mod dto;

use actix_web::{post, web, App, HttpResponse, HttpServer, Responder};
use game_server::state::Phase;

use crate::dto::InfoRes;

#[post("/info")]
async fn info(req: web::Json<dto::InfoReq>) -> impl Responder {
    let req = req.into_inner();

    let player_counts = {
        let mut pc = game_server::player_counts(req.settings)
            .into_iter()
            .collect::<Vec<_>>();
        pc.sort_unstable();
        pc
    };

    HttpResponse::Ok().json(InfoRes { player_counts })
}

#[post("/initial-state")]
async fn initial_state(req: web::Json<dto::InitialStateReq>) -> impl Responder {
    let req = req.into_inner();
    match game_server::initial_state(req.settings, req.players, req.seed) {
        Some(state) => HttpResponse::Ok().json(dto::InitialStateRes { state }),
        None => HttpResponse::UnprocessableEntity().finish(),
    }
}

#[post("/perform-action")]
async fn perform_action(req: web::Json<dto::PerformActionReq>) -> impl Responder {
    match req
        .state
        .perform_action(&req.settings.mode, &req.performed_by, &req.action, req.seed)
    {
        Some(next_state) => HttpResponse::Ok().json(dto::PerformActionRes {
            completed: next_state.phase == Phase::GameComplete,
            next_state,
        }),
        None => HttpResponse::UnprocessableEntity().finish(),
    }
}

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    let port: u16 = std::env::var("PORT").unwrap().parse().unwrap();
    let addr = format!("0.0.0.0:{}", port);
    println!("listening on {}", addr);
    HttpServer::new(|| {
        App::new()
            .service(info)
            .service(initial_state)
            .service(perform_action)
    })
    .bind(addr)?
    .run()
    .await
}
