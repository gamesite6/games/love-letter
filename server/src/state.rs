use crate::action::Action;
use crate::card::{CardFace, CardId};
use crate::settings::GameMode;

use crate::PlayerId;
use rand::prelude::*;
use serde::{Deserialize, Serialize};
use std::cmp;
use std::collections::{HashMap, HashSet};
use typescript_definitions::TypeScriptify;

#[derive(Clone, Debug, Deserialize, Serialize, TypeScriptify)]
pub struct State {
    pub deck: Deck,
    pub set_aside: CardId,
    pub face_up: Option<[CardId; 3]>,
    pub players: Vec<Player>,
    pub phase: Phase,

    #[ts(ts_type = "{ [playerId: number]: Speech }")]
    pub speech: HashMap<PlayerId, Speech>,
}

impl State {
    fn setup_next_round(
        &mut self,
        mode: &GameMode,
        previous_winner: &PlayerId,
        mut rng: &mut impl rand::Rng,
    ) {
        self.speech.clear();
        self.deck = Deck::new(mode, &mut rng);
        self.set_aside = self.deck.draw_card().unwrap();
        self.face_up = if self.players.len() == 2 {
            Some([
                self.deck.draw_card().unwrap(),
                self.deck.draw_card().unwrap(),
                self.deck.draw_card().unwrap(),
            ])
        } else {
            None
        };

        for player in self.players.iter_mut() {
            player.protected = false;
            player.hand.clear();
            player.hand.push(self.deck.draw_card().unwrap());
            player.discards.clear();
        }

        let drawn_card = self.deck.draw_card().unwrap();
        let starting_player = self.get_player_mut(previous_winner).unwrap();
        starting_player.hand.push(drawn_card);

        self.phase = Phase::Choose {
            actor: starting_player.id,
        };
    }

    fn get_next_actor_mut(&mut self, actor_id: &PlayerId) -> Option<&mut Player> {
        let next_actor_id = self.get_next_actor_id(actor_id)?;
        self.players
            .iter_mut()
            .find(|player| player.id == next_actor_id)
    }
    fn get_next_actor_id(&self, actor_id: &PlayerId) -> Option<PlayerId> {
        let actor_idx = self
            .players
            .iter()
            .position(|player| player.id == *actor_id)?;

        self.players
            .iter()
            .cycle()
            .skip(actor_idx + 1)
            .take(self.players.len() - 1)
            .find(|player| player.is_alive())
            .map(|player| player.id)
    }

    fn get_player(&self, player_id: &PlayerId) -> Option<&Player> {
        self.players.iter().find(|player| player.id == *player_id)
    }
    fn other_players<'a>(&'a self, player_id: &'_ PlayerId) -> impl Iterator<Item = &'a Player> {
        let player_id = *player_id;
        self.players
            .iter()
            .filter(move |player| player.id != player_id)
    }
    fn get_player_mut(&mut self, player_id: &PlayerId) -> Option<&mut Player> {
        self.players
            .iter_mut()
            .find(|player| player.id == *player_id)
    }

    pub fn perform_action(
        &self,
        mode: &GameMode,
        actor_id: &PlayerId,
        action: &Action,
        seed: i64,
    ) -> Option<Self> {
        if !self.action_is_allowed(actor_id, action).unwrap_or(false) {
            return None;
        }
        let mut rng = rand_pcg::Pcg32::seed_from_u64(seed.unsigned_abs());

        let actor_id = *actor_id;

        let mut result = self.clone();

        match (&self.phase, action) {
            (Phase::Choose { .. }, Action::Choose { played }) => {
                let played_card = {
                    let actor = result.get_player_mut(&actor_id)?;
                    let card = actor.hand.remove(*played);
                    actor.discards.push(card);
                    card.face()
                };

                result.speech.clear();

                if played_card.must_target_other_player()
                    && self
                        .other_players(&actor_id)
                        .all(|player| player.protected || !player.is_alive())
                {
                    result
                        .speech
                        .insert(actor_id, Speech::NoEffect { card: played_card });

                    result.give_turn_to_next_player(&actor_id);
                } else {
                    result
                        .speech
                        .insert(actor_id, Speech::PlayCard { card: played_card });

                    match played_card {
                        CardFace::RockStar => {
                            let actor_mut = result.get_player_mut(&actor_id)?;
                            actor_mut.discard_hand();
                            result.give_turn_to_next_player(&actor_id);
                        }
                        CardFace::Hacker => {
                            result.phase = Phase::Swap { actor: actor_id };
                        }
                        CardFace::Manager => {
                            result.give_turn_to_next_player(&actor_id);
                        }
                        CardFace::Teacher => {
                            let first_card_opt = result.deck.draw_card();
                            let second_card_opt = result.deck.draw_card();

                            let actor = result.get_player_mut(&actor_id)?;
                            if let Some(card) = first_card_opt {
                                actor.hand.push(card);
                            }
                            if let Some(card) = second_card_opt {
                                actor.hand.push(card);
                            }
                            if actor.hand.len() > 1 {
                                result.phase = Phase::Draw2 { actor: actor_id }
                            }
                        }
                        CardFace::Mechanic => {
                            result.phase = Phase::Discard { actor: actor_id };
                        }
                        CardFace::Doctor => {
                            let actor_mut: &mut Player = result.get_player_mut(&actor_id)?;
                            actor_mut.protected = true;
                            result.give_turn_to_next_player(&actor_id);
                        }
                        CardFace::Judge => {
                            result.phase = Phase::Compare { actor: actor_id };
                        }
                        CardFace::Detective => {
                            result.phase = Phase::Peek { actor: actor_id };
                        }
                        CardFace::Security => {
                            result.phase = Phase::Guess { actor: actor_id };
                        }
                        CardFace::Ninja => {
                            result.give_turn_to_next_player(&actor_id);
                        }
                    }
                }
            }
            (Phase::Swap { .. }, &Action::Swap { target }) => {
                result.speech.clear();
                result.speech.insert(actor_id, Speech::Swap { target });

                let mut temp: Vec<CardId> = result.get_player(&target)?.hand.clone();

                let actor_hand: &mut Vec<CardId> = &mut result.get_player_mut(&actor_id)?.hand;
                std::mem::swap(&mut temp, actor_hand);

                let target_hand: &mut Vec<CardId> = &mut result.get_player_mut(&target)?.hand;
                std::mem::swap(&mut temp, target_hand);

                result.give_turn_to_next_player(&actor_id);
            }
            (Phase::Draw2 { .. }, Action::Draw2 { returned }) => {
                let hand = &mut result.get_player_mut(&actor_id)?.hand;
                let returned_card = hand.remove(*returned);
                let hand_size = hand.len();
                result.deck.put_card_at_bottom(returned_card);

                if hand_size == 1 {
                    result.give_turn_to_next_player(&actor_id);
                }
            }
            (Phase::Discard { .. }, &Action::Discard { target }) => {
                result.speech.clear();
                result.speech.insert(actor_id, Speech::Discard { target });

                let target_mut: &mut Player = result.get_player_mut(&target)?;
                match target_mut.discard_hand() {
                    DiscardResult::NothingToDiscard => { /* should never happen */ }
                    DiscardResult::OutOfRound => { /* target is out. do not draw */ }
                    DiscardResult::Harmless => {
                        let drawn: Option<CardId> = result.deck.draw_card();
                        let set_aside = result.set_aside;
                        let target_mut: &mut Player = result.get_player_mut(&target)?;
                        if let Some(card) = drawn {
                            target_mut.hand.push(card);
                        } else {
                            target_mut.hand.push(set_aside);
                        }
                    }
                }
                result.give_turn_to_next_player(&actor_id);
            }

            (Phase::Compare { .. }, &Action::Compare { target }) => {
                result.speech.clear();
                result.speech.insert(actor_id, Speech::Compare { target });

                result.phase = Phase::CompareComplete {
                    actor: actor_id,
                    actor_ready: false,
                    target,
                    target_ready: false,
                };
            }
            (
                &Phase::CompareComplete {
                    actor,
                    actor_ready,
                    target,
                    target_ready,
                    ..
                },
                Action::CompareComplete,
            ) => {
                let actor_ready = actor_ready || actor_id == actor;
                let target_ready = target_ready || actor_id == target;

                if actor_ready && target_ready {
                    let actor_card = result.get_player(&actor)?.hand.first()?.face();
                    let target_card = result.get_player(&target)?.hand.first()?.face();

                    match actor_card.cmp(&target_card) {
                        cmp::Ordering::Greater => {
                            let target_mut: &mut Player = result.get_player_mut(&target)?;
                            let card = target_mut.hand.remove(0);
                            target_mut.discards.push(card);
                        }
                        cmp::Ordering::Less => {
                            let actor_mut: &mut Player = result.get_player_mut(&actor)?;
                            let card = actor_mut.hand.remove(0);
                            actor_mut.discards.push(card);
                        }
                        cmp::Ordering::Equal => { /* do nothing! neither player is eliminated */ }
                    }

                    result.give_turn_to_next_player(&actor);
                } else {
                    result.phase = Phase::CompareComplete {
                        actor,
                        actor_ready,
                        target,
                        target_ready,
                    }
                }
            }
            (&Phase::Peek { .. }, &Action::Peek { target }) => {
                result.speech.clear();
                result.speech.insert(actor_id, Speech::Peek { target });

                let target_card = result.get_player(&target)?.hand.first()?;
                result.phase = Phase::PeekComplete {
                    actor: actor_id,
                    target,
                    card: target_card.face(),
                };
            }
            (Phase::PeekComplete { .. }, Action::PeekComplete) => {
                result.give_turn_to_next_player(&actor_id);
            }
            (Phase::Guess { .. }, &Action::Guess { target, guess }) => {
                let target_card = *result.get_player(&target)?.hand.first()?;

                result.speech.clear();
                result
                    .speech
                    .insert(actor_id, Speech::Guess { target, guess });
                result.speech.insert(
                    target,
                    Speech::GuessResponse {
                        correct: target_card.face() == guess,
                    },
                );

                if target_card.face() == guess {
                    let target_mut: &mut Player = result.get_player_mut(&target)?;
                    target_mut.discard_hand();
                }
                result.give_turn_to_next_player(&actor_id);
            }
            (Phase::RoundComplete { winners, .. }, Action::RoundComplete) => {
                let ready = if let Phase::RoundComplete { ref mut ready, .. } = result.phase {
                    ready.insert(actor_id);
                    ready
                } else {
                    return None;
                };

                let all_ready = result
                    .players
                    .iter()
                    .all(|player| ready.contains(&player.id));

                if all_ready {
                    if result.is_goal_reached() {
                        result.phase = Phase::GameComplete
                    } else {
                        let previous_winner = winners
                            .iter()
                            .choose(&mut rng)
                            .expect("there must always be at least one winner");

                        result.setup_next_round(mode, previous_winner, &mut rng)
                    }
                }
            }

            _ => (),
        }

        Some(result)
    }

    pub fn is_goal_reached(&self) -> bool {
        let points_required = match self.players.len() {
            2 => 6,
            3 => 5,
            4 => 4,
            _ => 3,
        };

        self.players
            .iter()
            .any(|player| player.score >= points_required)
    }
    fn players_alive(&self) -> impl Iterator<Item = &Player> {
        self.players.iter().filter(|player| !player.hand.is_empty())
    }
    fn round_scorers(&self) -> (HashSet<PlayerId>, Option<PlayerId>) {
        let players_alive: Vec<&Player> = self.players_alive().collect();

        let winners = if players_alive.len() == 1 {
            players_alive
                .iter()
                .map(|player| player.id)
                .collect::<HashSet<PlayerId>>()
        } else {
            let max_card_value: CardFace = players_alive
                .iter()
                .flat_map(|player| player.hand.iter().map(|c| c.face()))
                .max()
                .unwrap();

            players_alive
                .iter()
                .filter(|player| player.hand.iter().any(|c| c.face() == max_card_value))
                .map(|player| player.id)
                .collect()
        };

        let players_with_spy = players_alive
            .iter()
            .filter(|player| player.discards.iter().any(|c| c.face() == CardFace::Ninja))
            .collect::<Vec<_>>();

        let spy_scorer = if players_with_spy.len() == 1 {
            Some(players_with_spy.first().unwrap().id)
        } else {
            None
        };

        (winners, spy_scorer)
    }

    fn give_turn_to_next_player(&mut self, current_actor_id: &PlayerId) {
        let players_alive = self
            .players
            .iter()
            .filter(|player| player.is_alive())
            .count();

        let next_card_opt: Option<CardId> = self.deck.draw_card();
        let next_actor_opt: Option<&mut Player> = self.get_next_actor_mut(current_actor_id);

        match (next_card_opt, next_actor_opt) {
            (Some(card), Some(actor)) if players_alive > 1 => {
                actor.hand.push(card);
                actor.protected = false;
                self.phase = Phase::Choose { actor: actor.id }
            }
            _ => {
                // round is over
                let (winners, spy_scorer) = self.round_scorers();

                for player_id in winners.iter() {
                    self.get_player_mut(player_id).unwrap().score += 1;
                }
                for player_id in spy_scorer.iter() {
                    self.get_player_mut(player_id).unwrap().score += 1;
                }

                self.phase = Phase::RoundComplete {
                    winners: winners.clone(),
                    spy: spy_scorer,
                    ready: HashSet::new(),
                };
            }
        }
    }

    fn action_is_allowed(&self, &actor_id: &PlayerId, action: &Action) -> Option<bool> {
        let result =
            match (&self.phase, action) {
                (&Phase::Choose { actor }, Action::Choose { played }) => {
                    if actor_id != actor {
                        return Some(false);
                    }
                    let player = self.players.iter().find(|player| player.id == actor)?;
                    let cards = &player.hand;
                    let (played_card, kept_card) = match played {
                        0 => (cards[0], cards[1]),
                        1 => (cards[1], cards[0]),
                        _ => return Some(false),
                    };
                    kept_card.face() != CardFace::Manager
                        || (played_card.face() != CardFace::Mechanic
                            && played_card.face() != CardFace::Hacker)
                }
                (&Phase::Swap { actor }, &Action::Swap { target }) => {
                    actor == actor_id
                        && target != actor_id
                        && self.players.iter().any(|player| {
                            player.id == target && player.is_alive() && !player.protected
                        })
                }
                (&Phase::Draw2 { actor }, Action::Draw2 { returned }) => {
                    actor == actor_id && self.get_player(&actor)?.hand.len() > *returned
                }
                (&Phase::Discard { actor }, &Action::Discard { target }) => {
                    actor == actor_id
                        && self.players.iter().any(|player| {
                            player.id == target && player.is_alive() && !player.protected
                        })
                }
                (&Phase::Compare { actor }, &Action::Compare { target }) => {
                    actor == actor_id
                        && target != actor_id
                        && self.players.iter().any(|player| {
                            player.id == target && player.is_alive() && !player.protected
                        })
                }
                (
                    &Phase::CompareComplete {
                        actor,
                        actor_ready,
                        target,
                        target_ready,
                    },
                    Action::CompareComplete,
                ) => (actor_id == actor && !actor_ready) || (actor_id == target && !target_ready),
                (&Phase::Peek { actor }, &Action::Peek { target }) => {
                    actor == actor_id
                        && target != actor_id
                        && self.players.iter().any(|player| {
                            player.id == target && player.is_alive() && !player.protected
                        })
                }
                (&Phase::PeekComplete { actor, .. }, Action::PeekComplete) => actor == actor_id,
                (&Phase::Guess { actor }, &Action::Guess { target, .. }) => {
                    actor == actor_id
                        && target != actor_id
                        && self.players.iter().any(|player| {
                            player.id == target && player.is_alive() && !player.protected
                        })
                }
                (Phase::RoundComplete { .. }, Action::RoundComplete) => true,
                _ => false,
            };
        Some(result)
    }
}

#[derive(Debug, Clone, Serialize, Deserialize, TypeScriptify, PartialEq, Eq)]
pub struct Player {
    pub id: PlayerId,
    pub score: u8,
    pub hand: Vec<CardId>,
    pub discards: Vec<CardId>,
    pub protected: bool,
}

impl Player {
    pub fn new(id: PlayerId, hand: CardId) -> Self {
        Player {
            id,
            score: 0,
            hand: vec![hand],
            discards: Vec::new(),
            protected: false,
        }
    }

    fn is_alive(&self) -> bool {
        !self.hand.is_empty()
    }

    fn discard_hand(&mut self) -> DiscardResult {
        if self.hand.is_empty() {
            DiscardResult::NothingToDiscard
        } else {
            let card = self.hand.remove(0);
            self.discards.push(card);
            if card.face() == CardFace::RockStar {
                DiscardResult::OutOfRound
            } else {
                DiscardResult::Harmless
            }
        }
    }
}

enum DiscardResult {
    OutOfRound,
    Harmless,
    NothingToDiscard,
}
#[derive(Clone, Debug, Deserialize, Serialize, Eq, PartialEq, TypeScriptify)]
#[serde(tag = "kind")]
pub enum Phase {
    Choose {
        actor: PlayerId,
    },
    Guess {
        actor: PlayerId,
    },
    Peek {
        actor: PlayerId,
    },
    PeekComplete {
        actor: PlayerId,
        target: PlayerId,
        card: CardFace,
    },
    Compare {
        actor: PlayerId,
    },
    CompareComplete {
        actor: PlayerId,
        actor_ready: bool,
        target: PlayerId,
        target_ready: bool,
    },
    Discard {
        actor: PlayerId,
    },
    Draw2 {
        actor: PlayerId,
    },
    Swap {
        actor: PlayerId,
    },
    RoundComplete {
        winners: HashSet<PlayerId>,
        spy: Option<PlayerId>,
        ready: HashSet<PlayerId>,
    },
    GameComplete,
}

#[derive(Debug, Clone, Serialize, Deserialize, TypeScriptify)]
pub struct Deck(pub Vec<CardId>);

impl Deck {
    pub fn draw_card(&mut self) -> Option<CardId> {
        self.0.pop()
    }
    fn put_card_at_bottom(&mut self, card: CardId) {
        self.0.insert(0, card);
    }

    pub fn new(game_mode: &GameMode, mut rng: &mut impl rand::Rng) -> Self {
        let mut cards: Vec<u8> = match game_mode {
            GameMode::Modern => vec![
                vec![70, 80, 90],
                vec![60, 61],
                vec![50, 51],
                vec![40, 41],
                vec![30, 31],
                vec![20, 21],
                vec![10, 11, 12, 13, 14, 15],
                vec![1, 2],
            ]
            .concat(),
            GameMode::Classic => vec![
                vec![70, 80, 90],
                vec![50, 51],
                vec![40, 41],
                vec![30, 31],
                vec![20, 21],
                vec![10, 11, 12, 13, 14],
            ]
            .concat(),
        };

        cards.shuffle(&mut rng);
        let cards = cards.iter().map(|&c| CardId(c)).collect::<Vec<CardId>>();

        Deck(cards)
    }
}

#[derive(Clone, Debug, Deserialize, Serialize, TypeScriptify)]
#[serde(tag = "kind")]
pub enum Speech {
    PlayCard { card: CardFace },
    NoEffect { card: CardFace },
    Guess { target: PlayerId, guess: CardFace },
    GuessResponse { correct: bool },
    Peek { target: PlayerId },
    Compare { target: PlayerId },
    Discard { target: PlayerId },
    Swap { target: PlayerId },
}
