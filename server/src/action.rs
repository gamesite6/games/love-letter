use crate::card::CardFace;
use crate::PlayerId;
use serde::Deserialize;
use typescript_definitions::TypeScriptify;

#[derive(Debug, Clone, Deserialize, PartialEq, Eq, TypeScriptify)]
#[serde(tag = "kind")]
pub enum Action {
    Choose { played: usize },
    Swap { target: PlayerId },
    Draw2 { returned: usize },
    Discard { target: PlayerId },
    Compare { target: PlayerId },
    CompareComplete,
    Peek { target: PlayerId },
    PeekComplete,
    Guess { target: PlayerId, guess: CardFace },
    RoundComplete,
}
