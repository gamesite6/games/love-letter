use game_server::{action::Action, state::State, PlayerId, Settings};
use serde::Deserialize;
use serde::Serialize;

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct InitialStateReq {
    pub players: Vec<PlayerId>,
    pub settings: Settings,
    pub seed: i64,
}

#[derive(Debug, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct InitialStateRes {
    pub state: State,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct PerformActionReq {
    pub performed_by: PlayerId,
    pub action: Action,
    pub state: State,
    pub settings: Settings,
    pub seed: i64,
}

#[derive(Debug, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct PerformActionRes {
    pub completed: bool,
    pub next_state: State,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct InfoReq {
    pub settings: Settings,
}

#[derive(Debug, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct InfoRes {
    pub player_counts: Vec<usize>,
}
