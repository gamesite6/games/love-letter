use serde::{Deserialize, Serialize};
use std::collections::HashSet;
use typescript_definitions::TypeScriptify;

#[derive(Debug, Clone, Serialize, Deserialize, TypeScriptify)]
#[serde(rename_all = "camelCase")]
pub struct Settings {
    pub mode: GameMode,
    pub selected_player_counts: HashSet<usize>,
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize, TypeScriptify)]
pub enum GameMode {
    Classic,
    Modern,
}
