fn main() {
    #[cfg(debug_assertions)]
    {
        use game_server as lib;
        use typescript_definitions::TypeScriptifyTrait;

        println!("export type PlayerId = number;\n");
        println!("export type CardId = number;\n");
        println!("{}\n", lib::settings::Settings::type_script_ify());
        println!("{}\n", lib::settings::GameMode::type_script_ify());
        println!("{}\n", lib::action::Action::type_script_ify());
        println!("{}\n", lib::state::State::type_script_ify());
        println!("{}\n", lib::state::Player::type_script_ify());
        println!("{}\n", lib::state::Speech::type_script_ify());
        println!("{}\n", lib::state::Deck::type_script_ify());
        println!("{}\n", lib::state::Phase::type_script_ify());
        println!("{}\n", lib::card::CardFace::type_script_ify());
    }
}
