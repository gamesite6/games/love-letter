use serde::{Deserialize, Serialize};

use typescript_definitions::TypeScriptify;

#[derive(Debug, Clone, Copy, Serialize, Deserialize, TypeScriptify, PartialEq, Eq)]
pub struct CardId(pub u8);

impl CardId {
    pub fn face(&self) -> CardFace {
        CardFace::from(self)
    }
}

impl From<&CardId> for CardFace {
    fn from(card_id: &CardId) -> Self {
        match card_id.0 {
            0..=9 => CardFace::Ninja,
            10..=19 => CardFace::Security,
            20..=29 => CardFace::Detective,
            30..=39 => CardFace::Judge,
            40..=49 => CardFace::Doctor,
            50..=59 => CardFace::Mechanic,
            60..=69 => CardFace::Teacher,
            70..=79 => CardFace::Hacker,
            80..=89 => CardFace::Manager,
            _ => CardFace::RockStar,
        }
    }
}

#[derive(
    Debug, Clone, Copy, PartialEq, Eq, Ord, PartialOrd, Serialize, Deserialize, TypeScriptify,
)]
pub enum CardFace {
    #[serde(rename = "C0")]
    Ninja,
    #[serde(rename = "C1")]
    Security,
    #[serde(rename = "C2")]
    Detective,
    #[serde(rename = "C3")]
    Judge,
    #[serde(rename = "C4")]
    Doctor,
    #[serde(rename = "C5")]
    Mechanic,
    #[serde(rename = "C6")]
    Teacher,
    #[serde(rename = "C7")]
    Hacker,
    #[serde(rename = "C8")]
    Manager,
    #[serde(rename = "C9")]
    RockStar,
}

impl CardFace {
    pub fn value(&self) -> u8 {
        *self as u8
    }
    pub fn must_target_other_player(&self) -> bool {
        matches!(
            self,
            CardFace::Hacker | CardFace::Judge | CardFace::Detective | CardFace::Security
        )
    }
}

#[cfg(test)]
mod card_tests {
    use super::*;

    #[test]
    fn card_ord() {
        assert!(CardFace::RockStar > CardFace::Manager)
    }
}
