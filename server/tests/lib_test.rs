#[cfg(test)]
mod tests {
    use game_server::action::*;
    use game_server::card::*;
    use game_server::state::*;
    use game_server::*;
    use std::collections::{HashMap, HashSet};

    #[test]
    fn test_create_game() {
        let players = vec![1, 2, 3];

        let settings = Settings {
            mode: GameMode::Modern,
            selected_player_counts: [2, 3, 4, 5].iter().copied().collect::<HashSet<_>>(),
        };
        let game = initial_state(settings, players, 123).unwrap();
        let first_player = game.players.iter().next().unwrap();

        assert_eq!(first_player.hand.len(), 2);
        assert_eq!(
            game.phase,
            Phase::Choose {
                actor: first_player.id
            }
        );
    }

    #[test]
    fn out_after_playing_9() {
        let game = State {
            deck: Deck(vec![CardId(30), CardId(0), CardId(80)]),
            set_aside: CardId(70),
            face_up: None,

            players: vec![
                Player {
                    id: 1,
                    score: 0,
                    discards: vec![],
                    hand: vec![CardId(90), CardId(10)],
                    protected: false,
                },
                Player {
                    id: 2,
                    score: 0,
                    discards: vec![],
                    hand: vec![CardId(11)],
                    protected: false,
                },
                Player {
                    id: 3,
                    score: 0,
                    discards: vec![],
                    hand: vec![CardId(12)],
                    protected: false,
                },
            ],
            phase: Phase::Choose { actor: 1 },
            speech: HashMap::new(),
        };

        let next_state = game
            .perform_action(&GameMode::Modern, &1, &Action::Choose { played: 0 }, 123)
            .expect("expected a next state!");

        assert_eq!(
            next_state.players[0],
            Player {
                id: 1,
                score: 0,
                discards: vec![CardId(90), CardId(10)],
                hand: vec![],
                protected: false,
            }
        );

        assert_eq!(next_state.phase, Phase::Choose { actor: 2 });
    }
}
